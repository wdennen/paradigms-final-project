// main js script -- wdennen & toconn22

console.log('page load - entered main.js FINAL PROJECT');
SPORTS = ['nfl', 'mlb', 'nhl', 'nba']

var get_users_button = document.getElementById('get-users-button');
get_users_button.onmouseup = usersApiWrapper;

var get_post_button = document.getElementById('post-user-button');
get_post_button.onmouseup = postUsers;

// function to add a new user given new username and desired sport, then displays new list of users below
function postUsers() {
    console.log('entered editUsers');
    var url = "http://localhost"
    var port = "8080"
    var user = document.getElementById('post-user').value;
    var sport    = document.getElementById('post-sport').value;
    var action = "POST";
    var final_url = url + ':' + port + '/sports/' + sport + '/users';
    var message_body = {'name': user}; // create message body

    var xhr = new XMLHttpRequest();
    xhr.open(action, final_url, true);
    
    xhr.onload = function(e) {
        console.log(xhr.responseText);
        getUsersApi(sport);     // display new user list
    }
    xhr.onerror = function(e) {
        console.log("ERROR");
        console.error(xhr.statusText);
    }
    xhr.send(JSON.stringify(message_body));
    console.log(sport);
}

// loop through the four sports and hit the api for each one
function usersApiWrapper() {
    for (sport of SPORTS) {
        getUsersApi(sport)
    }
}

// hits the api and gets the response of all users in given sport
function getUsersApi(sport){
    console.log('entered getUsersApi');
    var url = "http://localhost"
    var port = "8080"
    var action = "GET"
    var xhr = new XMLHttpRequest();
    var final_url = url + ':' + port + '/sports/' + sport + '/users';
    console.log(final_url)
    xhr.open(action, final_url, true);
    xhr.onload = function(e) {
        console.log(xhr.responseText);
        updateUsersLabel(xhr.responseText, sport)
    }
    xhr.onerror = function(e) {
        console.error(xhr.statusText);
    }
    xhr.send(null);
}

// after hitting api we update html elements and display users
function updateUsersLabel(response_text, sport) {
    document.getElementById(sport + '-user-div').innerHTML=sport + ' users'; // clear previous results before new results displayed
    console.log('entered updateUsersLabel');
    var response_json = JSON.parse(response_text);
    ul = document.createElement('ul');
    document.getElementById(sport + '-user-div').appendChild(ul);
    var user_set = new Set(response_json['users']); 
    for (user of user_set) {
        let li = document.createElement('li');
        ul.appendChild(li);
        li.innerHTML += user;
    }
}