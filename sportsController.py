'''
Thomas O'Connor: toconn22
William Dennen: wdennen
'''

import cherrypy
import json
from sports_library import _sports_database

class SportController(object):
        def __init__(self, sdb=None):
                if sdb is None:
                        self.sdb = _sports_database()
                else:
                        self.sdb = sdb

                self.sdb.load_sports()

        def GET_INDEX(self):
                output = {'result':'success'}
                sports_list = self.sdb.get_sports()
                output['sports'] = sports_list
                words = {} # top words for each sport
                posts = {} # top post for each sport
                for s in sports_list:
                            words[s] = self.sdb.get_words(s)
                            posts[s] = self.sdb.get_top(s)

                output['words'] = words
                output['posts'] = posts

                return json.dumps(output)


        def GET_SPORT(self, sport):
                # returns top post and most used words for sport
                output = {'result':'success'}
                try:
                        output['words'] = self.sdb.get_words(sport)
                        output['post'] = self.sdb.get_top(sport)
                except Exception as e:
                        output['result'] = 'error'
                        output['message'] = str(e)


                return json.dumps(output)


        def GET_TOP(self, sport):
                # returns top post for given sport
                output = {'result': 'success'}
                try:
                        output['post'] = self.sdb.get_top(sport)
                except Exception as e:
                        output['result'] = 'error'
                        output['message'] =  str(e)
               
                return json.dumps(output)


        def GET_USERS(self, sport):
                # returns list of users for given sport
                output = {'result':'success'}
                try:
                        output['users'] = self.sdb.get_users(sport)
                except Exception as ex:
                        output['result'] = 'error'
                        output['message'] = str(ex)

                return json.dumps(output)


        def PUT_USER(self, sport):
                # adds a user for a given sport
                data = json.loads(cherrypy.request.body.read().decode('utf-8'))
                
                output = {'result' : 'success'}
                change = None
                try:
                        change = self.sdb.set_user(sport, data['name'], data['new_name'])
                except Exception as e:
                        output['result'] = 'error'
                        output['message'] = str(e)
                
                if not change:
                        output['result'] = 'error'

                return json.dumps(output)

        
        def POST_USER(self, sport):
                # changes a given user in sport's username
                data = json.loads(cherrypy.request.body.read().decode('utf-8'))
                output = {'result' : 'success'}
                
                try:
                        self.sdb.add_user(sport, data['name'])
                except Exception as e:
                        output['result'] = 'error'
                        output['message'] = str(e)

                return json.dumps(output)


        def DELETE_USER(self, sport, user):
                # deletes a user in a sport
                output = {'result': 'success'}
                change = None
                try:
                        change = self.sdb.del_user(sport, user)
                except Exception as e:
                        output['result'] = 'error'
                        output['message'] = str(e)
                
                if not change:
                        output['result'] = 'error'

                return json.dumps(output)
            
        def DELETE_INDEX(self):
                # deletes entire database
                self.sdb.del_data()

                output = {'result': 'success'}
                return json.dumps(output)

        def RESET_INDEX(self):
            # resets entire database
            self.sdb.reset_index()
            output = {'result' : 'success'}
            return json.dumps(output)

