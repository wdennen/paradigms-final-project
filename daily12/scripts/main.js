// respond to button click
console.log("Page load happened!")

pButton1 = document.getElementById('bsr-one-button');
pButton1.onmouseup = buttonone;
pButton2 = document.getElementById('bsr-two-button');
pButton2.onmouseup = buttontwo;
pButton3 = document.getElementById('bsr-three-button');
pButton3.onmouseup = buttonthree;
var submitButton = document.getElementById('bsr-submit-button')
submitButton.onmouseup = getFormInfo;

function buttonone(){
    var prog_bar = document.getElementById("prog-bar");
    prog_bar.style.width = "33%"

}
function buttontwo(){
    var prog_bar = document.getElementById("prog-bar");
    prog_bar.style.width = "66%"

}
function buttonthree(){
    var prog_bar = document.getElementById("prog-bar");
    prog_bar.style.width = "100%"

}
function getFormInfo(){
    console.log("Entered get Form Info!")
    
    // get checkbox state
    var boxes_count = 0;
    if (document.getElementById('checkbox-horror-value').checked){
        console.log('detected horror!');
        boxes_count += 1;
    }

    if (document.getElementById('checkbox-comedy-value').checked) {
        console.log('detected comedy!');
        boxes_count += 1;
    }

    if (document.getElementById('checkbox-drama-value').checked) {
        console.log('detected drama!');
        boxes_count += 1;
    }
    // make checkboxes combined string
    console.log('genres: ' + boxes_count.toString());

    var sample_text = document.getElementById('sample-text').value;

    // make dictionary
    basic_dict = {};
    basic_dict['boxes'] = boxes_count;
    basic_dict['text'] = sample_text;
    
    console.log(basic_dict);

    displayStory(basic_dict);

}

function displayStory(basic_dict){
    console.log('entered displayStory!');
    console.log(basic_dict);
    // get fields from story and display in label.
    var story_top = document.getElementById('checkboxes-text');
    story_top.innerHTML = basic_dict['boxes'];

    var text_body = document.getElementById('text-body');
    text_body.innerHTML = basic_dict['text'];

}
