import json
from collections import Counter

class _sports_database:
    def __init__(self):
        self.sport_names = ['mlb', 'nba', 'nhl', 'nfl']
        self.users = dict() # users on each page
        self.top_posts = dict() # top post from each page
        self.top_words = dict() # top words used for each page

    def gen_top(self, all_words):
        counter = Counter(all_words)
        most_often = counter.most_common(5)
        return most_often

    def load_sports(self):
        mlb = open('mlb.json')
        nba = open('nba.json')
        nhl = open('nhl.json')
        nfl = open('nfl.json')

        sports = [mlb, nba, nhl, nfl]

        for i,f in enumerate(sports):
             # get users
             top_score = -1
             top_post = ''
             all_words = []
             users = []
             cur_sport = json.load(f)
             posts = cur_sport['data']['children'] 
             for child in posts:
                 p = child['data']
                 users.append(p['author'])
                 if p['score'] > top_score:
                     top_score = p['score']
                     top_post =  p['title']
                 all_words += p['title'].split()
             sport_name = self.sport_names[i]
             self.users[sport_name] = users
             self.top_posts[sport_name] = {'title': top_post, 'score': top_score}
             self.top_words[sport_name] = self.gen_top(all_words)

        mlb.close()
        nba.close()
        nhl.close()
        nfl.close()
    

    def get_sports(self):
        return self.sport_names


    def get_users(self, sport):
        return self.users[sport]

    
    def get_top(self, sport):
        return self.top_posts[sport]


    def get_words(self, sport):
        return self.top_words[sport]

    
    def set_user(self, sport, username, change):
        try:
            loc = self.users[sport].index(username)

            self.users[sport][loc] = change

            return change
        except ValueError:
            return None



    def add_user(self, sport, username):
        self.users[sport].append(username)
    

    def del_user(self, sport, username):
        try:
            self.users[sport].remove(username)

            return username
        except ValueError:
            return None



    ''' BE CAREFUL USING THIS '''
    def del_data(self):
        self.sport_names = []
        self.users.clear()
        self.top_words.clear()
        self.top_posts.clear()


if __name__ == "__main__":
       sdb = _sports_database()
       sdb.load_sports()
