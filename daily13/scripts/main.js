// respond to button click
console.log("Page load happened!")

pButton1 = document.getElementById('bsr-one-button');
pButton1.onmouseup = buttonone;
pButton2 = document.getElementById('bsr-two-button');
pButton2.onmouseup = buttontwo;
pButton3 = document.getElementById('bsr-three-button');
pButton3.onmouseup = buttonthree;
var submitButton = document.getElementById('bsr-submit-button')
submitButton.onmouseup = getFormInfo;

function buttonone(){
    var prog_bar = document.getElementById("prog-bar");
    prog_bar.style.width = "33%"

}
function buttontwo(){
    var prog_bar = document.getElementById("prog-bar");
    prog_bar.style.width = "66%"

}
function buttonthree(){
    var prog_bar = document.getElementById("prog-bar");
    prog_bar.style.width = "100%"

}
function getFormInfo(){
    console.log("Entered get Form Info!")
    
    // get checkbox state
    var boxes_count = 0;
    if (document.getElementById('checkbox-horror-value').checked){
        console.log('detected horror!');
        boxes_count += 1;
    }

    if (document.getElementById('checkbox-comedy-value').checked) {
        console.log('detected comedy!');
        boxes_count += 1;
    }

    if (document.getElementById('checkbox-drama-value').checked) {
        console.log('detected drama!');
        boxes_count += 1;
    }
    // make checkboxes combined string
    console.log('genres: ' + boxes_count.toString());

    var sample_text = document.getElementById('sample-text').value;

    // make dictionary
    basic_dict = {};
    basic_dict['boxes'] = boxes_count;
    basic_dict['text'] = sample_text;
    
    console.log(basic_dict);

    displayStory(basic_dict);

}

function displayStory(basic_dict){
    console.log('entered displayStory!');
    console.log(basic_dict);
    // get fields from story and display in label.
    makeNetworkCallToBeerApi(basic_dict['boxes'].toString());

    

}

function makeNetworkCallToBeerApi(beerId){
    console.log('entered make nw rrrr' + beerId);
    // set up url
    var xhr = new XMLHttpRequest(); // 1 - creating request object
    var url = "https://api.punkapi.com/v2/beers?ids=" + beerId;
    xhr.open("GET", url, true); // 2 - associates request attributes with xhr

    // set up onload
    xhr.onload = function(e) { // triggered when response is received
        // must be written before send
        console.log(xhr.responseText);
        // do something
        updateBeerWithResponse(beerId, xhr.responseText);
    }

    // set up onerror
    xhr.onerror = function(e) { // triggered when error response is received and must be before send
        console.error(xhr.statusText);
    }

    // actually make the network call
    xhr.send(null) // last step - this actually makes the request

} // end of make nw call

function updateBeerWithResponse(beerId, response_text){
    var response_json = JSON.parse(response_text);
    console.log(response_json)
    // update a label
    var label1 = document.getElementById("response-line1");

    if(response_json[0]['description'] == null){
        label1.innerHTML = 'Apologies, we could not find your name.'
    } else{
        label1.innerHTML =  'Beer: ' + response_json[0]['description'];
        var name = response_json[0]['name'];
        makeNetworkCallToNames(name);
    }
}

function makeNetworkCallToNames(name){
    console.log('entered make nw call' + name);
    // set up url
    var xhr = new XMLHttpRequest(); // 1 - creating request object
    var url = "https://api.genderize.io/?name=" + name.split(' ')[0];
    xhr.open("GET", url, true) // 2 - associates request attributes with xhr

    // set up onload
    xhr.onload = function(e) { // triggered when response is received
        // must be written before send
        console.log(xhr.responseText);
        // do something
        updateGenderWithResponse(name, xhr.responseText);
    }

    // set up onerror
    xhr.onerror = function(e) { // triggered when error response is received and must be before send
        console.error(xhr.statusText);
    }

    // actually make the network call
    xhr.send(null) // last step - this actually makes the request

} // end of make nw call

function updateGenderWithResponse(name, response_text){
    // update a label
    var response_json = JSON.parse(response_text);

    console.log("entered update gender")
    console.log(response_text)
    //var label2 = document.getElementById("response-line2");
    //label2.innerHTML = response_text;
    name_summary = "Beer Name: " + name + ", Beer Gender: " + response_json["gender"] + "\n";
    // dynamically adding label
    label_item = document.createElement("label"); // "label" is a classname
    label_item.setAttribute("id", "dynamic-label" ); // setAttribute(property_name, value) so here id is property name of button object

    var item_text = document.createTextNode(name_summary); // creating new text
    //label_item.appendChild(item_text); // adding something to button with appendChild()

    // option 1: directly add to document
    // adding label to document
    //document.body.appendChild(label_item);

    // option 2:
    // adding label as sibling to paragraphs
    var response_div = document.getElementById("response-div");
    response_div.appendChild(item_text);

} // end of updateTriviaWithResponse


