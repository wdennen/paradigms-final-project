'''
authors: toconn22 and wdennen
'''


import cherrypy
from sportsController import SportController
from sports_library import _sports_database

class optionsController:
    def options(self, *args, **kwargs):
        return ""

def CORS():
    cherrypy.response.headers['Access-Control-Allow-Origin'] = '*'
    cherrypy.response.headers['Access-Control-Allow-Methods'] = 'GET, PUT, POST, DELETE, OPTIONS'
    cherrypy.response.headers['Access-Control-Allow-Credentials'] = 'true'
def start_service():
    dispatcher = cherrypy.dispatch.RoutesDispatcher()

    sdb = _sports_database()

    sportController     = SportController(sdb=sdb) 

    dispatcher.connect('sports_index_get', '/sports/', controller=sportController, action = 'GET_INDEX', conditions=dict(method=['GET']))
    dispatcher.connect('sport_get', '/sports/:sport', controller=sportController, action = 'GET_SPORT', conditions=dict(method=['GET']))
    dispatcher.connect('top_get', '/sports/:sport/top', controller=sportController, action = 'GET_TOP', conditions=dict(method=['GET']))
    dispatcher.connect('users_get', '/sports/:sport/users', controller=sportController, action = 'GET_USERS', conditions=dict(method=['GET']))
    dispatcher.connect('user_put', '/sports/:sport/users', controller=sportController, action = 'PUT_USER', conditions=dict(method=['PUT']))
    dispatcher.connect('user_post', '/sports/:sport/users', controller=sportController, action = 'POST_USER', conditions=dict(method=['POST']))

    dispatcher.connect('user_delete', '/sports/:sport/users/:user', controller=sportController, action = 'DELETE_USER', conditions=dict(method=['DELETE']))
    dispatcher.connect('sports_index_delete', '/sports/', controller=sportController, action = 'DELETE_INDEX', conditions=dict(method=['DELETE']))
    dispatcher.connect('sports_index_reset', '/sports/reset', controller=sportController, action = 'RESET_INDEX', conditions=dict(method=['PUT']))


    conf = {
	'global': {
            'server.thread_pool': 5, # optional argument
	    'server.socket_host': 'localhost', # 
	    'server.socket_port': 8080, #change port number to your assigned
	    },
	'/': {
	    'request.dispatch': dispatcher,
            'tools.CORS.on':True,
	    }
    }

    cherrypy.config.update(conf)
    app = cherrypy.tree.mount(None, config=conf)
    cherrypy.quickstart(app)

# end of start_service


if __name__ == '__main__':
    cherrypy.tools.CORS = cherrypy.Tool('before_finalize', CORS)
    start_service()
