// toconn22 and wdennen

anychart.onDocumentReady(function(){
    var xhrnfl = new XMLHttpRequest();
    var xhrmlb = new XMLHttpRequest();
    var xhrnhl = new XMLHttpRequest();
    var xhrnba = new XMLHttpRequest();
    xhrnfl.open("GET", 'http://localhost:8080/sports/nfl', true);
    xhrmlb.open("GET", 'http://localhost:8080/sports/mlb', true);
    xhrnba.open("GET", 'http://localhost:8080/sports/nba', true);
    xhrnhl.open("GET", 'http://localhost:8080/sports/nhl', true);
    

    // NFL
    xhrnfl.onload = function(e) {
        var obj = JSON.parse(xhrnfl.responseText);
        var data = obj.words;
        var chart = anychart.tagCloud(data);
        chart.title("Top Words Used");
        chart.angles([0]);
        chart.container('nfl-cloud-container');
        chart.draw();

        var title = obj.post.title;
        var score = obj.post.score;
        document.getElementById("nfl-post").innerHTML = title + ' [' + score + ']';
    }

    xhrnfl.onerror = function(e) {
        console.error(xhrnfl.statusText);
    }

    xhrnfl.send(null);
    
    // MLB
    xhrmlb.onload = function(e) {
        var obj = JSON.parse(xhrmlb.responseText);
        var data = obj.words;
        var chart = anychart.tagCloud(data);
        chart.title("Top Words Used");
        chart.angles([0]);
        chart.container('mlb-cloud-container');
        chart.draw();
        var title = obj.post.title;
        var score = obj.post.score;
        document.getElementById("mlb-post").innerHTML = title + ' [' + score + ']';
    }

    xhrmlb.onerror = function(e) {
        console.error(xhrmlb.statusText);
    }

    xhrmlb.send(null);
    
    // NHL
    xhrnhl.onload = function(e) {
        var obj = JSON.parse(xhrnhl.responseText);
        var data = obj.words;
        var chart = anychart.tagCloud(data);
        chart.title("Top Words Used");
        chart.angles([0]);
        chart.container('nhl-cloud-container');
        chart.draw();
        var title = obj.post.title;
        var score = obj.post.score;
        document.getElementById("nhl-post").innerHTML = title +  ' [' + score + ']';
    }

    xhrnhl.onerror = function(e) {
        console.error(xhrnhl.statusText);
    }

    xhrnhl.send(null);
    
    // NBA
    xhrnba.onload = function(e) {
        var obj = JSON.parse(xhrnba.responseText);
        var data = obj.words;
        var chart = anychart.tagCloud(data);
        chart.title("Top Words Used");
        chart.angles([0]);
        chart.container('nba-cloud-container');
        chart.draw();
        var title = obj.post.title;
        var score = obj.post.score;
        document.getElementById("nba-post").innerHTML = title +  ' [' + score + ']';
    }

    xhrnba.onerror = function(e) {
        console.error(xhrnba.statusText);
    }

    xhrnba.send(null);
});

