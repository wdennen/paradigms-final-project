'''
authors: toconn22 and wdennen
'''

import unittest
from sports_library import _sports_database

SDB = _sports_database()

class TestSportsApi(unittest.TestCase):

    def test_load(self):
        # make sure we can init out db
        SDB.load_sports()
        # check to se if we have loaded everything (users, posts, words)
        self.assertEqual(SDB.users['nfl'][0], 'nfl_gamethread')
        self.assertEqual(SDB.top_posts['nfl']['score'], 17400)
        self.assertEqual(SDB.top_words['mlb'][0], {'x': 'the', 'value': 11})
    
    def test_gen_top(self):
        # check to see if we can use counter for most used words
        test = 'the the the yo buddy test the cool tommy sweet'
        top = SDB.gen_top(test.split())
        self.assertEqual(top[0], {'x': 'the', 'value': 4})

    def test_get_sports(self):
        # test getting our names back
        SDB.reset_index()
        sports = SDB.get_sports()
        self.assertEqual(sports, ['mlb', 'nba', 'nhl', 'nfl'])

    def test_get_words(self):
        # test getting top words by sport
        SDB.reset_index()
        mlb_top = SDB.get_words('mlb')
        self.assertEqual(mlb_top[1], {'x': 'to', 'value': 8})

    def test_set_user(self):
        # test changing a user for a sport
        SDB.reset_index()
        
        # change Dorkside to tommy (Dorkside is first user)
        SDB.set_user('mlb', 'Dorkside', 'tommy')
        users = SDB.get_users('mlb')

        self.assertEqual(users[0], 'tommy')

    def test_add_user(self):
        # test adding a user to nfl
        SDB.reset_index()

        SDB.add_user('nfl', 'bob')
        users = SDB.get_users('nfl')
        self.assertEqual(users[-1], 'bob')

    def test_del_user(self):
        # test removal of mlb user
        SDB.reset_index()

        SDB.del_user('mlb', 'Dorkside')
        users = SDB.get_users('mlb')
        self.assertNotEqual(users[0], 'Dorkside')

    def test_del_data(self):
        # test removal of entire index
        SDB.reset_index()

        SDB.del_data()
        self.assertEqual(SDB.users, {})
        self.assertEqual(SDB.top_posts, {})
        self.assertEqual(SDB.top_words, {})







if __name__ == "__main__":
    unittest.main()

