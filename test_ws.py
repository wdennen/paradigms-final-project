import unittest
import requests
import json

class TestSportsWs(unittest.TestCase):

    SITE_URL = 'http://localhost:8080' # replace with your assigned port id
    print("Testing for server: " + SITE_URL)
    SPORTS_URL = SITE_URL + '/sports/'
    
    def reset_data(self):
        requests.put(self.SPORTS_URL + 'reset')

    def is_json(self, resp):
        try:
            json.loads(resp)
            return True
        except ValueError:
            return False

    def test_sports_index_get(self):
        self.reset_data()
        r = requests.get(self.SPORTS_URL)
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        sports = resp['sports']
        # check sports list
        self.assertEqual(sports, ['mlb', 'nba', 'nhl', 'nfl'])
        # check our words and list keys
        words = resp['words']
        posts = resp['posts']
        self.assertNotEqual(words, None)
        self.assertNotEqual(posts, None)


    def test_sports_get_sport(self):
        self.reset_data()

        mlb = requests.get(self.SPORTS_URL + '/mlb')
        nba = requests.get(self.SPORTS_URL + '/nba')
        nhl = requests.get(self.SPORTS_URL + '/nhl')
        nfl = requests.get(self.SPORTS_URL + '/nfl')

        self.assertTrue(self.is_json(mlb.content.decode()))
        self.assertTrue(self.is_json(nba.content.decode()))
        self.assertTrue(self.is_json(nhl.content.decode()))
        self.assertTrue(self.is_json(nfl.content.decode()))

        # check content of one of the responses
        mlb_r = json.loads(mlb.content.decode())
        # top words
        self.assertEqual(mlb_r['words'][0], {'x': 'the', 'value': 11})
        
        wrong = requests.get(self.SPORTS_URL + '/wrong')
        self.assertTrue(self.is_json(wrong.content.decode()))
        
        # make sure we get proper error message for bad sport
        wrong_r = json.loads(wrong.content.decode())
        self.assertEqual(wrong_r['result'], 'error')

    def test_sports_get_top(self):
        self.reset_data()
        
        r = requests.get(self.SPORTS_URL + '/nba/top')
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())

        # check to see if we get back correct top post by score
        self.assertEqual(resp['post']['score'], 10385)

    def test_sports_get_users(self):
        self.reset_data()
        
        r = requests.get(self.SPORTS_URL + '/nfl/users')
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())

        # check to see if we get back correct first user
        self.assertEqual(resp['users'][0], 'nfl_gamethread')

    def test_sports_put_users(self):
        self.reset_data()
        data = json.dumps({'name': 'nfl_gamethread', 'new_name': 'tommy'})
        r = requests.put(self.SPORTS_URL + '/nfl/users', data=data)
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())

        # check to see if we were able to change the first user
        r = requests.get(self.SPORTS_URL + '/nfl/users')
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['users'][0], 'tommy')

        # test user not found
        data = json.dumps({'name': 'wrong123', 'new_name':'tommy'})
        r = requests.put(self.SPORTS_URL + '/nfl/users', data=data)
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'error')

    def test_sports_post_users(self):
        self.reset_data()
        data = json.dumps({'name': 'test123'})
        r = requests.post(self.SPORTS_URL + '/nba/users', data=data)
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')

        # check to see if we were able to add a new user
        new_r = requests.get(self.SPORTS_URL + '/nba/users')
        new_resp = json.loads(new_r.content.decode())
        self.assertEqual(new_resp['users'][-1], 'test123')

    def test_sports_delete_users(self):
        self.reset_data()

        # delete the last user in nfl
        r = requests.delete(self.SPORTS_URL + '/nfl/users/DontTedOnMe')
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'],  'success')

        # check if last user is gone
        r = requests.get(self.SPORTS_URL + '/nfl/users')
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['users'][-1],  'DontTedOnMe')
    


    def test_sports_delete_index(self):
        self.reset_data()
        data = json.dumps({})
        r = requests.delete(self.SPORTS_URL, data=data)
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())

        # check to see if our index was cleared
        self.assertEqual(resp['result'], 'success')
        get = requests.get(self.SPORTS_URL)
        
        # will be 500 error not json since there is no content to get
        self.assertFalse(self.is_json(get.content.decode()))
        
        # see if data is restored
        self.reset_data()
        get = requests.get(self.SPORTS_URL)
        self.assertTrue(self.is_json(get.content.decode()))




        


if __name__ == "__main__":
    unittest.main()

