# FINAL PROJECT

API:
    The API stores the following:
        Users: It contains a list for each of our four sports (nfl, nhl, mlb, nba) of the users that are on the front page for each of those pages
        Top Posts: It contains the top post from each page's headline as well as that posts score
        Top Words: It contains a list of the top five most used words on each front pages headlines as well as their word count.
    The API lets users do the following:
        Delete, Load, and Reset the index.
        Get sports names, users, top posts, and most frequently used words.
        Change stored users usernames, add users, and delete users.
    To run API tests:
        In order to run api tests please run python3 test_api.py

Web Server:
    How to run server:
        Our server runs on localhost port 8080. To run python3 server.py
    What can users on our server do:
        GET /sports/ will return to our user basic info for each of the four sports
        GET /sports/:sport_name will return to our user top posts and most used words for given sports
        GET /sports/:sport/top will return just the top post and score for the given sport
        GET /sports/:sport/users will return the list of users for given sport
        PUT /sports/:sport/users will allow our user to change the username of a user in given sport
        POST /sports/:sport/users will allow user to add new user to given sport
        DELETE /sports/:sport/users/:user will delete the given user in given sport
        DELETE /sports/ will delete entire index
        PUT /sports/reset will reload all starting data

Frontend: (NOTE this all resides in assets folder..)
    HTML:
        The html is in one file for our fronted, final-index.html. This html although seems quite simple has some scripts embedded in it that allow for some cool scrolling and other features that make it look pretty nice. It also describes containers and divs that get filled with information from the javascript that makes calls to our API.
    JavaScript:
        Our javascript resides in two files, main.js and cloud.js. Main.js has the code for both our form, as well as the get users buttons. It makes api calls to our server to get the users, and also to add them. Cloud.js has the code for our word clouds as well as it places the top headline and score on the page.
    User-Interaction:
        1.) Users can click get Users at the top of the page and then at the bottom of the page see a list of users that made the front page for each sub reddit.
        2.) Users can add to that user list at the bottom of the page.
        3.) Users can look at the different sport subreddits by image, and see the top post for each of those subreddits as well as its score.
        4.) Users can also see the most frequently used words as a wordcloud for each subreddit. 
        5.) Users hover over each word in the cloud they can see details about how often that word was used.
    
Complexity:
    Our project has quite a bit of complexity. To start we are dealing with live data, as our .json files come from reddit subreddit pages that change very often. To account for this we have a function in our sports_library.py OOP library file that pulls the latest data to those files. We also have to parse this json data as the reddit api is not very advanced and we cannot simply call "get top posts", we have to manipualte the returned json ourselves to find this. We do more of this manipulation with our word cloud generation. We have to ourselves have a function that gets the most used words from each page. Our project is very scalable, as the json that is returned from any subreddit is in the same format. This means that we could hypothetically run our code on any subreddit we wanted and display the same analytics. We could also fairly easily use the data we get from those api calls to display other meaningful information we wanted.

SLIDES: https://docs.google.com/presentation/d/1fmIqm6C2YRq9bgMCPw6r08hKvCSRTOGLqgWX4YtGvcs/edit#slide=id.g5410c62a19_0_468

CODE WALKTHROUGH: https://www.youtube.com/watch?v=n9DLEr6cqzw&ab_channel=ThomasO%27Connor

DEMO VIDEO: https://www.youtube.com/watch?v=Ln5TsyX0oU0&feature=youtu.be


    
