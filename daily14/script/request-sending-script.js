// toconn22 and wdennen

console.log('page load - entered main.js for js-other api');

var send_button = document.getElementById('send-button');
send_button.onmouseup = getFormInfo;

var clear_button  = document.getElementById('clear-button');
clear_button.onmouseup = clearForm;

function getFormInfo(){
    console.log('entered getFormInfo!');

    // call get url_base
    var selindex = document.getElementById("select-server-address").selectedIndex;
    var url_base = document.getElementById('select-server-address').options[selindex].value;
    console.log('Host you entered is ' + url_base);
    
    // text input-port-number
    var port_num = document.getElementById('input-port-number').value;
    console.log('port is: ' + port_num);


    // radio buttons
    var action = "GET"; // default
    if (document.getElementById('radio-get').checked) {
        action = "GET";
    } else if (document.getElementById('radio-put').checked) {
        action = "PUT";
    } else if (document.getElementById('radio-post').checked) {
        action = "POST";
    } else if (document.getElementById('radio-delete').checked) {
        action = "DELETE";
    }
    console.log('Action is: ' + action);

    // key
    var key = null;
    if (document.getElementById('checkbox-use-key').checked) {
        key = document.getElementById('input-key').value;
    }
    
    // message_body
    var message_body = null;
    if (document.getElementById('checkbox-use-message').checked) {
        message_body = document.getElementById('text-message-body').value;
    }
    
    var xhr = new XMLHttpRequest();
    console.log('new base is ' + url_base);
    
    if (key) {
        xhr.open(action, url_base + ':' + port_num + '/movies/' + key, true);
    }
    else {
        xhr.open(action, url_base + ':' + port_num  +  '/movies/', true);
    }

    xhr.onload = function(e) {
        console.log(xhr.responseText);
        
        update_label(xhr.responseText);
    }

    xhr.onerror = function(e) {
        console.error(xhr.statusText);
    }

    if (message_body) {
        xhr.send(message_body);
    } else {
        xhr.send(null);
    }

    

} // end of get form info


function update_label(response_text) {
    document.getElementById('answer-label').innerHTML
        = response_text;
}


function clearForm(){
    window.location.reload();
} // end of make nw call

